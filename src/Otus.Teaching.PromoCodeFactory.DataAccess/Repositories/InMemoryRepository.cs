﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected List<T> Data { get; set; }

        public InMemoryRepository(List<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.AsEnumerable<T>());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }
        
        public Task<IEnumerable<T>> CreateAsync(T entity)
        {
         List<T> list = new List<T>();
         list.Add(entity);   
         Data.AddRange(list);
         return Task.FromResult(Data.AsEnumerable<T>());
        }

        public Task<IEnumerable<T>> DeleteAsync(Guid id)
        {
          Data.Remove(Data.First(c => (c.Id == id))) ;
          return Task.FromResult(Data.AsEnumerable<T>());
        }
       
       public Task<IEnumerable<T>> UpdateAsync(T entity)
       {
            var indexOf = Data.IndexOf(Data.Find(p => p.Id == entity.Id));
            Data[indexOf] = entity;
          // return Task.FromResult(Data.Where(S => S.Id == entity.Id)
           // .Select(S => { S = entity ; return S; }).ToList().AsEnumerable<T>());
            
        return Task.FromResult(Data.AsEnumerable<T>());

       }
           
    }
}